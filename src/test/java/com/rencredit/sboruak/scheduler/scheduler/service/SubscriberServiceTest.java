package com.rencredit.sboruak.scheduler.scheduler.service;

import com.rencredit.sboruak.scheduler.scheduler.config.ApplicationConfiguration;
import com.rencredit.sboruak.scheduler.scheduler.entity.Subscriber;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

import static org.junit.Assert.assertNotNull;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ApplicationConfiguration.class)
public class SubscriberServiceTest {

    @Autowired
    private SubscriberService subscriberService;

    @Before
    public void setup() {
        subscriberService.deleteAll();
    }

    @Test
    public void testFindAll() {
        Assert.assertEquals(0, subscriberService.findAll().size());
        subscriberService.save("mail_1");
        subscriberService.save("mail_2");
        Assert.assertEquals(2, subscriberService.findAll().size());
    }

}
