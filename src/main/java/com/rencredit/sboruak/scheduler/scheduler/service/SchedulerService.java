package com.rencredit.sboruak.scheduler.scheduler.service;

import com.rencredit.sboruak.scheduler.scheduler.entity.Subscriber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SchedulerService {

    @Autowired
    private EmailService emailService;

    @Autowired
    private SubscriberService subscriberService;

//    @Scheduled(fixedDelay = 1000)
//    public void scheduleFixedDelayTask() {
//        System.out.println("Fixed delay task - " + System.currentTimeMillis() / 1000);
//    }

    @Scheduled(cron = "0 0 17 * * ?")
    public void sendMessageGoDrinkTee() {
        List<Subscriber> subscriberList = subscriberService.findAll();
        for(Subscriber subscriber : subscriberList) {
            emailService.sendSimpleMessage(subscriber.getEmail(), "Tee", "Lets go drink tee");
        }
    }

//    @Scheduled(cron = "1 * * * * ?")
//    public void sendMessageEveryMinute() {
//        System.out.println("Mes send");
//        List<Subscriber> subscriberList = subscriberService.findAll();
//        for(Subscriber subscriber : subscriberList) {
//            emailService.sendSimpleMessage(subscriber.getEmail(), "Tee", "Lets go drink tee");
//        }
//    }

}
