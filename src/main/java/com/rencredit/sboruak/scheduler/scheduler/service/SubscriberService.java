package com.rencredit.sboruak.scheduler.scheduler.service;

import com.rencredit.sboruak.scheduler.scheduler.entity.Subscriber;
import com.rencredit.sboruak.scheduler.scheduler.repository.ISubscriberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class SubscriberService {

    @Autowired
    private ISubscriberRepository subscriberRepository;

    public Subscriber save(Subscriber s) {
        return subscriberRepository.save(s);
    }

    public void save(String email) {
        Subscriber subscriber = new Subscriber(email);
        subscriberRepository.save(subscriber);
    }

    public Subscriber findById(String id) {
        return subscriberRepository.findById(id).orElse(null);
    }

    public List<Subscriber> findAll() {
        return subscriberRepository.findAll();
    }

    public void deleteById(String s) {
        subscriberRepository.deleteById(s);
    }

    public void deleteAll() {
        subscriberRepository.deleteAll();
    }

}
