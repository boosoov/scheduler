package com.rencredit.sboruak.scheduler.scheduler.repository;

import com.rencredit.sboruak.scheduler.scheduler.entity.AbstractEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IAbstractRepositoryEntity<E extends AbstractEntity> extends JpaRepository<E, String> {

}
