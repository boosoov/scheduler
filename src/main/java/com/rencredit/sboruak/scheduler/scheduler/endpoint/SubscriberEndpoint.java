package com.rencredit.sboruak.scheduler.scheduler.endpoint;

import com.rencredit.sboruak.scheduler.scheduler.entity.Subscriber;
import com.rencredit.sboruak.scheduler.scheduler.service.EmailService;
import com.rencredit.sboruak.scheduler.scheduler.service.SubscriberService;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/subscriber")
public class SubscriberEndpoint {

    @Autowired
    private SubscriberService subscriberService;

    @Autowired
    private EmailService emailService;

    @PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public void create(@RequestBody Subscriber subscriber) {
        System.out.println(subscriber);
        subscriberService.save(subscriber);
    }

    @Nullable
    @GetMapping(value = "/{id}")
    public Subscriber findOneById(@PathVariable("id") String id) {
        System.out.println(id);
        Subscriber subscriber = subscriberService.findById(id);
        System.out.println(subscriber);
        return subscriber;
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Subscriber> getList() {
//        emailService.sendSimpleMessage("sboryak@rencredit.ru", "subject", "text");
        return subscriberService.findAll();
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteOneById(@PathVariable("id") String id) {
        subscriberService.deleteById(id);
    }

    @DeleteMapping("/all")
    public void deleteAll() {
        subscriberService.deleteAll();
    }

}
