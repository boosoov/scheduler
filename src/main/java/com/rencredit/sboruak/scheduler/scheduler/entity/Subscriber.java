package com.rencredit.sboruak.scheduler.scheduler.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(name = "app_subscriber")
public class Subscriber extends AbstractEntity {

    @Nullable
    @Column(name = "email")
    private String email;

    public Subscriber() {
    }

    public Subscriber(@Nullable String email) {
        this.email = email;
    }

}
