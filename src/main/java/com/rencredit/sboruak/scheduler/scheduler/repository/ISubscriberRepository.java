package com.rencredit.sboruak.scheduler.scheduler.repository;

import com.rencredit.sboruak.scheduler.scheduler.entity.Subscriber;
import org.springframework.stereotype.Repository;

@Repository
public interface ISubscriberRepository extends IAbstractRepositoryEntity<Subscriber> {
}
