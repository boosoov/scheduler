FROM openjdk:8-oraclelinux7
ADD target/scheduler-test-task.jar scheduler-test-task.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "scheduler-test-task.jar"]